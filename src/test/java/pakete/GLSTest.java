package pakete;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GLSTest
{
  @Test
  void testGLS()
  {
    //Configurator.setRootLevel(Level.DEBUG);
    GLS gls = new GLS();

    int price = gls.getPriceForDimensions(30, 15, 8, 1000);

    assertEquals(430, price);
  }
}